//Limpiar toda la tabla
$(document).ready(function() {
    $("#btnlimpiar").click(function() {
                if (result.value) 
            {
                $("input").val(""); //Limpiar todas las cajas de texto
                $("date").val(""); //Limpiar todas las fechas
                $("select").val(""); //Limpiar todos los select optio
            }
    });
});


//VALIDAR BOTON REGISTRAR
$(document).ready(function() {
    $("#btncalcular").click(function() {
        let validado = 0;
        console.log(validado);
            
        //Validar el tipo de inmueble
        if ($("#tipo_inmueble").val().length == 0) {
            $("#mensaje_tipo_inmueble").text("*");
        } else {
            $("#mensaje_tipo_inmueble").text("");
            console.log("validado");
            validado++;
        }

           //Validar Ciudad
           if ($("#ciudad").val().length == 0) {
            $("#mensaje_ciudad").text("*");
        }

         else if ($("#ciudad").val().length > 30) {
           Swal.fire({
               icon: 'error',
               title: 'Oops...',
               text: 'EL número de caracteres de la Ciudad no debe ser mayor a 30.'
             })
       } else {
            $("#mensaje_ciudad").text("");
            validado++;
        }

       //Validar Dirección
     if ($("#direccion").val().length == 0) {
        $("#mensaje_direccion").text("*");
    } else {
        $("#mensaje_direccion").text("");
        console.log("validado");
        validado++;
    }

          //Validar Fecha de Construccion (date)
          if($("#fecha").val().length==0)
          {
              $("#mensaje_fecha").text("*");
          
          }
          else {
              $("#mensaje_fecha").text("");
              validado++;
             }
     
             let fechaactual = new Date();
             let fecha = new Date($("#fecha").val()+" 00:00:00"); //Capturar fecha
             console.log(fechaactual+"======"+fecha);
             if(fecha > fechaactual)
             {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'La fecha de Construcción no puede ser mayor a la actual.'
                  })
             }
                  
    
    //Validar Area Metros cuadrados
    if ($("#area").val().length == 0) {
        $("#mensaje_area").text("*");
    }else if ($("#area").val() <20 || $("#area").val() >1000) {
        Swal.fire({
           icon: 'error',
           title: 'Oops...',
           text: 'Digite un Area entre 20 y 1000.'
         })
       } else 
    {
        $("#mensaje_area").text("");
        validado++;

    } if (validado == 5) {
            Swal.fire({
                icon: 'success',
                title: 'Éxito',
                text: 'Datos guardados correctamente!'
            })
        }
        else {
            Swal.fire({
                position: 'top-end',
                icon: 'error',
                title: 'Dedes completar todos los campos.',
                showConfirmButton: false,
                timer: 1200
            })
        }
        return false;
    });
});

      //Validaciones para digitar solo numeros 
      $(document).ready(function()
      { 
          $(".solo_numeros").keyup(function(){
          this.value = this.value.replace(/[^0-9]/g,''); //Expresion regular    
      });
     
          $(".solo_numeros").keypress(function(){
          this.value = this.value.replace(/[^0-9]/g,''); //Expresion regular 
      });
  
          $(".solo_numeros").keydown(function(){
          this.value = this.value.replace(/[^0-9]/g,''); //Expresion regular 
      });
  
  
    //VALIDAR SOLO LETRAS
    $(".solo_letras").on("keypress",function(){
        $input = $(this); //captura la caja de textp
        $input.val($input.val().toUpperCase());//convierte en mayuscula
    });
    $(".solo_letras").on("keyup",function(){ 
        $input = $(this); //captura la caja de textp
        $input.val($input.val().toUpperCase());//convierte en mayuscula
    });
    $(".solo_letras").on("keydown",function(){  
        $input = $(this); //captura la caja de textp
        $input.val($input.val().toUpperCase());//convierte en mayuscula
    });
    $(".solo_letras").on("blur",function(){ //el onblur es para convertir la validacion después de salirse de la caja de texto  
        $input = $(this); //captura la caja de textp
        $input.val($input.val().toUpperCase());//convierte en mayuscula
    });
});

$(document).ready(function () {
    $('.solo_numeros').on('input', function () {
        this.value = (this.value + '').replace(/[^0-9]/g, '');
    });
    $('.solo_letras').keyup(function () {
        this.value = (this.value + '').replace(/[^A-Z,Ñ," "]/g, '');
    });
    $('.solo_letras').keypress(function () {
        this.value = (this.value + '').replace(/[^A-Z,Ñ," "]/g, '');
    });

    $('.solo_letras').keydown(function () {
        this.value = (this.value + '').replace(/[^A-Z,Ñ," "]/g, '');
    });

});

   function calcular_valores() {
    var tipo=document.getElementById('tipo_inmueble').value;
    var metros=document.getElementById('area').value;
    var valor1=80000;
    var valor2=90000;
    var dolar=3921;
    var valor_inmueble=0;
    var dolares=0;
    if(tipo=="apto"){
        valor_inmueble=valor1*metros;
        dolares=valor_inmueble/dolar;
        document.getElementById('valor_metro').value =valor1;
        document.getElementById('valor_inmueble_cop').value =valor_inmueble;
        document.getElementById('valor_inmueble_usd').value =dolares;
    }else if(tipo=="casa"){
        valorinmueble=valor2*metros;
        dolares=valorinmueble/dolar;
        document.getElementById('valor_metro').value = valor2;
        document.getElementById('valor_inmueble_cop').value =valor_inmueble;
        document.getElementById('valor_inmueble_usd').value =dolares;
    }
};

 